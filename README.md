# Smartmarket #

Smartmarket is a simple Android app that integrates with Mercado Libre (Latin America biggest online marketplace) REST APIs.
Although it is easy for buyers to calculate shipping costs and estimated delivery time in Mercado Libre, they insist to ask it for sellers. This app helps you to speed up this repetitive task.
Smartmarket automatically retrieves all questions a seller receive, presents it in a intuitive UI, and can build question's answer text containing shipping costs and estimated delivery time.
